# Curs R Avançat

Possible Curs d'R Avançat a donar en un futur. Actualment és una pluja d'idees...

(Reordenar com es vulgui, i/o separar en nivell intermig i avançat)

## Consultar datasets de CityOS
Xavier: https://gitlab.ajuntament.bcn/omd-gid/1812_ser_161_gid_consulta_produccio_cityos/blob/master/Peticio161.Rmd#L131

## Spark i R amb sparklyr
Ús eficient dels recursos del CityOS per consultes tipus BigData des de R via Spark:
Xavier: https://gitlab.com/radup/curs-r-introduccio/blob/master/codi/extra.tips.bigdata.R

CityOS?: https://ajuntament.barcelona.cat/digital/sites/default/files/adt-open_source_open_technology_af.pdf

## DBplyr: accedir a bd grans amb llenguatge tidyverse i no morir en l'intent
Especialment pensat per a accés a bd amb taules grans (cityos, oracle padro, ....). Ús eficient dels recursos del CityOS per consultes tipus (quasi) BigData des de R via Impala:

Xavier: https://gitlab.ajuntament.bcn/omd-gid/1812_ser_161_gid_consulta_produccio_cityos/blob/master/Peticio161.Rmd#L167

CityOS?: https://ajuntament.barcelona.cat/digital/sites/default/files/adt-open_source_open_technology_af.pdf

## Gestió segura de contrasenyes amb R
Xavier: 
* https://gitlab.ajuntament.bcn/omd-gid/1901_pro_181_omd_preparar_dades_vodafone_i_d_altres_pel_bsc/blob/master/Peticio181.R#L115
* https://gitlab.com/xavidp/1806_Python_for_Data_Analysis/blob/master/R_Twitter_TM/R_Twitter_TM.Rmd#L36
* https://omd-gid.imi.bcn/180608-Proposta-Gestio-Contrasenyes-OMD-Keepass

## Combinació de taules per similitud de clau comú amb stringdist_joins
Xavier: https://gitlab.ajuntament.bcn/omd-gid/1902_pro_191_gid_paquet_omd.bd/blob/master/omd.bd/inst/rmarkdown/templates/ajbcn_cerca_dades/skeleton/skeleton.Rmd#L1750

## Gràfics interactius avançats amb htmlwidgets
Xavier: https://gitlab.com/radup/SAIER_Urgencies/blob/master/SAIER_urgencies.Rmd

## Paralel.lització (parallel, snowfall, doMC, foreach, ...)
Xavier: https://github.com/xavidp/bpeva/blob/master/eva_main.R

## Apply* efficients amb future.apply
Xavier: https://gitlab.com/radup/curs-r-introduccio/blob/master/codi/extra.tips.efficiency.R

## Benchmarking: Comparació del rediment de processos
Xavier: https://gitlab.com/radup/curs-r-introduccio/blob/master/comparativa_lectura_escriptura.Rmd#L134

## Profiling: Detecció dels punts lents del codi
http://adv-r.had.co.nz/Profiling.html

## Debugging: Mètodes i eines per facilitar la depuració d'errades de codi
Intro: https://support.rstudio.com/hc/en-us/articles/205612627-Debugging-with-RStudio
Advanced: http://adv-r.had.co.nz/Exceptions-Debugging.html

## Creació de paquets d'R
Xavier: 
* https://gitlab.ajuntament.bcn/omd-gid/1902_pro_190_gid_paquet_omd.tractaments
* https://gitlab.ajuntament.bcn/omd-gid/1902_pro_191_gid_paquet_omd.bd
* https://gitlab.ajuntament.bcn/omd-gid/1902_pro_195_gid_paquet_omd.tools

## Elaboració de plantilles Rmd
Xavier: https://gitlab.ajuntament.bcn/omd-gid/1902_pro_191_gid_paquet_omd.bd/tree/master/omd.bd/inst/rmarkdown/templates

## Programes web amb r (shiny, tiki, ... )
Xavier: https://r.tiki.org/r_test03_interface

## Web Spider/Crawler: agafar arbre de continguts remots
Xavier: https://github.com/xavidp/boleteRa/blob/master/meteo.R

## Web scraping with email notifications: notificar de canvis en pagines html 
Xavier: https://github.com/xavidp/webchanges/blob/master/NotifyWebChanges.R

## Extreure dades tabulars a partir de pdf
Xavier: https://github.com/xavidp/rscripts/blob/master/tabulizer_summer_school_ub_2016.R

## Extreure dades en text via ocr a partir de documents escanejats
https://cran.r-project.org/web/packages/tesseract/vignettes/intro.html
https://ropensci.org/technotes/2018/11/06/tesseract-40/

## Batch image processing: Processat d'imatges per lots 
https://cran.r-project.org/web/packages/magick/vignettes/intro.html

Xavier: https://gitlab.com/radup/SAIER_Urgencies/blob/master/SAIER_urgencies.Rmd#L676

## Consultes a l'API de Twitter des de R
Xavier: https://gitlab.com/xavidp/1806_Python_for_Data_Analysis/blob/master/R_Twitter_TM/R_Twitter_TM.Rmd

## Telegram Bots: programar i gestionar un canal de Telegram des de R
Xavier: https://gitlab.com/xavidp/1806_Python_for_Data_Analysis/blob/master/R_Telegram_Bot/R_Telegram_Bot.Rmd

## Reverse Geocoding amb R: obtenir informacio de bd a partir de dades geolocalitzades
Xavier: https://github.com/xavidp/boleteRa/blob/master/boleteRa.Rmd#L209

## Nozzle.R1: Framework d'Informes avançats (html, js, pdf) recurrents provinent de la Bioinformàtica
Xavier: https://github.com/xavidp/bpmmirna/blob/master/basicA.Rmd#L150

## ....





Relacionat:
* https://gitlab.com/radup/curs-r-introduccio
